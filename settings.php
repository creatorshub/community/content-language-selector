//<?php

$form->add( new \IPS\Helpers\Form\Text( 'plugin_example_setting', \IPS\Settings::i()->plugin_example_setting ) );

if ( $values = $form->values() )
{
	$form->saveAsSettings();
	return TRUE;
}

return $form;

/*
 * Clubs - Content Language Selector
 * Clubs - Content Language Where Clause
 * Activity Feed - Only include content language
 * Search - Only include content language
 * ACP - Select Content Language for Forum
 * Content Language Aware Topics Feed Widget
 * Guest Caching Problem
 * Caching
 */