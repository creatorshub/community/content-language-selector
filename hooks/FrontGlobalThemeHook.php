//<?php

/* To prevent PHP errors (extending class does not exist) revealing path */
if ( !defined( '\IPS\SUITE_UNIQUE_KEY' ) )
{
	exit;
}

class hook17 extends _HOOK_CLASS_
{

/* !Hook Data - DO NOT REMOVE */
public static function hookData() {
 return array_merge_recursive( array (
  'footer' => 
  array (
    0 => 
    array (
      'selector' => '#elNavLang',
      'type' => 'add_before',
      'content' => '{template="contentLangBoardFooter" group="plugins" location="global" app="core" params="$languages"}',
    ),
    1 => 
    array (
      'selector' => '#elNavLang',
      'type' => 'replace',
      'content' => '<a href="#elNavLang_menu" id="elNavLang" data-ipsmenu="" data-ipsmenu-above="">
{lang=\'contentLanguage_UiLanguage\'}
<i class="fa fa-caret-down"> </i>
</a>',
    ),
  ),
  'userBar' => 
  array (
    0 => 
    array (
      'selector' => '#elUserNav',
      'type' => 'add_inside_end',
      'content' => '{{if ( \IPS\Settings::i()->site_online || \IPS\Member::loggedIn()->group[\'g_access_offline\'] ) and (\IPS\Dispatcher::i()->application instanceof \IPS\Application AND \IPS\Dispatcher::i()->application->canAccess() )}}
    {{$languages = \IPS\Lang::getEnabledLanguages();}}
	{{if count( $languages ) > 1}}
	{template="contentLangBoardTop" group="plugins" location="global" app="core" params="$languages"}
	{{endif}}
{{endif}}',
    ),
  ),
), parent::hookData() );
}
/* End Hook Data */


}
