//<?php

/* To prevent PHP errors (extending class does not exist) revealing path */
if ( !defined( '\IPS\SUITE_UNIQUE_KEY' ) )
{
	exit;
}

class hook13 extends _HOOK_CLASS_
{
    /**
     * @param $permissionCheck
     * @param $member
     * @param $where
     *
     * @return array
     */
    private static function addWhereContentLang($permissionCheck, $member, $where): array
    {
        if ($permissionCheck) {
            $toCheck = ! is_null($member) ? $member : \IPS\Member::loggedIn();

            $where[] = [
                'ch_content_language=?',
                self::getContentLanguage($toCheck)
            ];

            return $where;
        }

        return $where;
    }

    /**
     * @param $toCheck
     *
     * @return int
     */
    private static function getContentLanguage($toCheck): int
    {
        if($toCheck->member_group_id == \IPS\Settings::i()->guest_group)
        {
            if(array_key_exists('ch_content_language', \IPS\Request::i()->cookie))
            {
                return (int) \IPS\Request::i()->cookie['ch_content_language'];
            }

            return 1;
        }

        if($toCheck->ch_content_language)
        {
            return $toCheck->ch_content_language;
        }

        return 1;
    }

    /**
     * [Node] Add/Edit Form
     *
     * @param    \IPS\Helpers\Form $form The form
     *
     * @return    void
     */
    public function form(&$form)
    {
        parent::form($form);

        $languages = [];
        foreach (\IPS\Lang::getEnabledLanguages() as $id => $lang) {
            $languages[$id] = $lang->title;
        }

        // add content language selector
        $form->add(
            new \IPS\Helpers\Form\Select(
                'ch_content_language',
                $this->ch_content_language,
                true,
                array('options'  => $languages, 'multiple' => false)
            ),
            'forum_name',
            'forum_settings'
        );
        //die(var_dump($form));
    }

    /**
     * Fetch All Root Nodes
     *
     * @param    string|NULL      $permissionCheck The permission key to check for or NULl to not check permissions
     * @param    \IPS\Member|NULL $member          The member to check permissions for or NULL for the currently logged in member
     * @param    mixed            $where           Additional WHERE clause
     *
     * @return    array
     */
    static public function roots($permissionCheck = 'view', $member = null, $where = [])
    {
        $where = self::addWhereContentLang($permissionCheck, $member, $where);

        return call_user_func_array('parent::roots', [$permissionCheck, $member, $where]);
    }

    static public function nodesWithPermission($permissionCheck, $member, $where = [])
    {
        $where = self::addWhereContentLang($permissionCheck, $member, $where);

        return call_user_func_array('parent::nodesWithPermission', array($permissionCheck, $member, $where));
    }
}
