//<?php

/* To prevent PHP errors (extending class does not exist) revealing path */
if ( !defined( '\IPS\SUITE_UNIQUE_KEY' ) )
{
	exit;
}

class hook18 extends _HOOK_CLASS_
{
    public function contentLanguage()
    {
        if(\IPS\Request::i()->requestMethod() !== "POST")
        {
            \IPS\Output::i()->error('form_bad_method', 'X', 400, '');
            return;
        }

        \IPS\Session::i()->csrfCheck();

        if ( \IPS\Member::loggedIn()->member_id )
        {
            \IPS\Member::loggedIn()->ch_content_language = (int) \IPS\Request::i()->id;
            \IPS\Member::loggedIn()->save();
        }
        else
        {
            \IPS\Request::i()->setCookie( 'ch_content_language', (int) \IPS\Request::i()->id );
        }

        \IPS\Output::i()->redirect(
            isset($_SERVER['HTTP_REFERER'])
                ? $_SERVER['HTTP_REFERER']
                : \IPS\Http\Url::internal('')
        );
    }
}
