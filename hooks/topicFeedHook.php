//<?php

/* To prevent PHP errors (extending class does not exist) revealing path */
if ( !defined( '\IPS\SUITE_UNIQUE_KEY' ) )
{
	exit;
}

class hook14 extends _HOOK_CLASS_
{
	/**
	 * Get where clause
	 *
	 * @return	array
	 */
	protected function buildWhere()
	{
        $where = parent::buildWhere();

        $where['container'][] = array(
            'forums_forums.ch_content_language=?',
            \IPS\Member::loggedIn()->ch_content_language ?? 1
        );

        return $where;
	}

}
