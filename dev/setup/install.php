//<?php


/* To prevent PHP errors (extending class does not exist) revealing path */
if ( !defined( '\IPS\SUITE_UNIQUE_KEY' ) )
{
	header( ( isset( $_SERVER['SERVER_PROTOCOL'] ) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0' ) . ' 403 Forbidden' );
	exit;
}

/**
 * Install Code
 */
class ips_plugins_setup_install
{
	/**
	 * ...
	 *
	 * @return	array	If returns TRUE, upgrader will proceed to next step. If it returns any other value, it will set this as the value of the 'extra' GET parameter and rerun this step (useful for loops)
	 */
	public function step1()
	{
        if(!\IPS\Db::i()->checkForColumn('core_members', 'ch_content_language')){
            \IPS\Db::i()->addColumn( 'core_members', array(
                'name'		=> 'ch_content_language',
                'type'		=> 'MEDIUMINT',
                'length'	=> 4,
                'null'		=> FALSE,
                'default'	=> 1,
                'comment'	=> 'the language the user prefers for his content consumption'
            ) );
        }

        if(!\IPS\Db::i()->checkForColumn('forums_forums', 'ch_content_language')){
            \IPS\Db::i()->addColumn( 'forums_forums', array(
                'name'		=> 'ch_content_language',
                'type'		=> 'MEDIUMINT',
                'length'	=> 4,
                'null'		=> FALSE,
                'default'	=> 1,
                'comment'	=> 'the language the user prefers for his content consumption'
            ) );
        }

		return TRUE;
	}
	
	// You can create as many additional methods (step2, step3, etc.) as is necessary.
	// Each step will be executed in a new HTTP request
}